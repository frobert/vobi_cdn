<?php 
class tx_vobi_cdn {
	function hook_strreplace(&$params, &$obj)    {
		$config = $obj->config['config']['tx_vobicdn.'];

		if($config['ignoreHttps'] === "1" && $_SERVER['HTTPS'] === "on")
			return;

		if($config['enable'] === "1") {
			if(!$config || !isset($config['search.']) || !$config['search.'] || !isset($config['replace.']) || !$config['replace.'])
				return;
		
			$params['search']  += $config['search.'];
			$params['replace'] += $config['replace.'];
		} else
			return;
	}
	
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/vobi_cdn/Classes/class.tx_vobicdn_cdn.php'])    {
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/vobi_cdn/Classes/class.tx_vobicdn_string_cdn.php']);
}
?>