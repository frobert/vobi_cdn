Extension Manual
=================

- install extension
- configure typoscript

Example: 

Constants:
cdnDomain = http://cdn.domain.com/

Setup:
config.tx_vobicdn {
	enable = 0
	ignoreHttps = 0
	search {
		1 = "typo3temp/
		2 = "fileadmin/
		3 = "uploads/
	}

	replace {
		1 = "{$cdnDomain}typo3temp/
		2 = "{$cdnDomain}fileadmin/
		3 = "{$cdnDomain}uploads/
	}
}