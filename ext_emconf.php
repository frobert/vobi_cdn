<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "vobi_cdn"
 *
 * Auto generated by Extension Builder 2013-12-06
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'CDN',
	'description' => 'Content delivery network',
	'category' => 'misc',
	'author' => 'Robert Ferencek',
	'author_email' => 'robert@vobi.si',
	'author_company' => 'VOBI.si',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '1.1.0',
	'constraints' => array(
		'depends' => array(
			'extbase' => '6.0',
			'fluid' => '6.0',
			'typo3' => '6.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>