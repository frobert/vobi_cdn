<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY);
require_once($extPath.'Classes/class.tx_vobicdn_cdn.php');


$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['tslib_fe-contentStrReplace'][] = 'tx_vobi_cdn->hook_strreplace';

?>